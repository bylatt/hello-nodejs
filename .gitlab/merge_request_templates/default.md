<!-- Merge request template -->

**Jira ticket:** (Link)

**Description**

> Description

**Changes**

- Please list down the changes

**Prerequisites**

- Please list down the required changes for this PR, eg: environment variable, feature flags, etc.

**Related issues**

- Please list down related Jira tickets if available\*\*

**Type of change**

- [ ] New feature (adds functionality)
- [ ] Bug fix (fixes an issue)
- [ ] Breaking change (fix or feature that would cause existing functionality to change)
- [ ] Refactor, non-breaking change
- [ ] Handle automation tests
- [ ] Configuration, non-breaking change

**Checklists**

- [ ] Lint rules passed locally
- [ ] All new and existing tests passed.
- [ ] Application changes have been tested thoroughly
- [ ] My code follows the style guidelines of this project
- [ ] I have performed a self-review of my own code
- [ ] I have commented on my code, particularly in hard-to-understand areas

**Proof of work or screenshots (if appropriate):**
