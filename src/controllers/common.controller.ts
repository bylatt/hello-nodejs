import { Request, Response } from "express";
import httpStatus from "http-status";

export default {
  hello: (_: Request, res: Response) =>
    res.status(httpStatus.OK).json({ msg: "hello world" }),
};
