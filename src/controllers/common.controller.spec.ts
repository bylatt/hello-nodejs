import { jest, describe, test, expect } from "@jest/globals";
import { getMockReq, getMockRes } from "@jest-mock/express";
import httpStatus from "http-status";
import commonController from "./common.controller";

describe("common controller", () => {
  test("hello", () => {
    const mockReq = getMockReq();
    const { res: mockRes } = getMockRes({ json: jest.fn(), status: jest.fn() });
    commonController.hello(mockReq, mockRes);
    expect(mockRes.status).toHaveBeenCalledWith(httpStatus.OK);
    expect(mockRes.json).toHaveBeenCalledWith({ msg: "hello world" });
  });
});
