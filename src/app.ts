import express, { Express } from "express";
import helmet from "helmet";
import cors from "cors";

import commonController from "./controllers/common.controller";

const app: Express = express();
const port: number = 3000;

app.use(helmet());
app.use(cors());
app.get("/", commonController.hello);

app.listen(port, () => console.log(`App listen at :${port}`));
